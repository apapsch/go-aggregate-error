# go-aggregate-error

Return multiple errors from your Go function.

Example:

``` go
func Foo() error {
    errs := aggregaterr.NewError()
    for {
        // Do something that returns error but should not break the loop.
        err := foo()
        if err != nil {
            errs.Append(err)
            continue
        }
    }
    return errs.Return()
}
```

`Error.Return` returns `nil` if no error was appended. Otherwise it returns
itself.
