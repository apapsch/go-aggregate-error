package aggregaterr

import (
	"errors"
	"testing"
)

func TestAggregateError(t *testing.T) {
	err := Error{
		Prefix: "foo: ",
		Errors: []error{
			errors.New("no good"),
			errors.New("bugged"),
		},
	}
	if err.Error() != "foo: no good; bugged" {
		t.Fatal("unexpected error message")
	}
}

func TestAppend(t *testing.T) {
	err := New("foo: ")
	err.Append(errors.New("very bad"))
	if err.Error() != "foo: very bad" {
		t.Fatal("unexpected error message")
	}
}

func TestReturn(t *testing.T) {
	agg := New("foo: ")
	err := agg.Return()
	if err != nil {
		t.Fatalf("did not expect error from empty aggregate error, but got %v", err)
	}
}
