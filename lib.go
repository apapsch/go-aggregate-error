// Package aggregaterr provides an error type storing multiple errors.
package aggregaterr

import "strings"

// Error allows returning several errors while satisfying the
// error interface.
type Error struct {
	// Prefix is used in the error message before any of the
	// aggregated errors.
	Prefix string
	// Errors are used in the error message by concatenating their
	// message.
	Errors []error
}

func New(prefix string) *Error {
	return &Error{
		Prefix: prefix,
		Errors: make([]error, 0),
	}
}

func (a *Error) Append(err error) {
	a.Errors = append(a.Errors, err)
}

// Error returns messages from all errors concatenated with semicolon.
func (a *Error) Error() string {
	errs := make([]string, 0, len(a.Errors))
	for _, err := range a.Errors {
		errs = append(errs, err.Error())
	}
	return a.Prefix + strings.Join(errs, "; ")
}

func (a *Error) Return() error {
	if len(a.Errors) == 0 {
		return nil
	}
	return a
}
